/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techbrothers.techsolutions.springbootfreemarker.controllers;

import com.techbrothers.techsolutions.springbootfreemarker.config.DisplayProeprties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author chiru
 */
@Controller
public class DisplayController {
    
    @Autowired
    DisplayProeprties displayProeprties;
       
    @GetMapping("/display")
    public String display(Model model){
        model.addAttribute("firstName", displayProeprties.getUserFirstName());
        model.addAttribute("lastName", displayProeprties.getUserLastName());
        return "display";
    }
    
}
